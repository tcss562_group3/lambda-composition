###Transformation AB

Replace BUCKET_NAME, KEY, FILENAME, OUTPUT_KEY and compress along with dependencies.

OUTPUT_KEY needs to be <bucketname>/<newfile>
This OUTPUT_KEY file will be input to next transformation TRANSFORM_C,
make sure you update the transformation_c parameters accordingly.

For invoking lambda with a lambda, a custom role has to be created for "lambdaAB" lambda function.
These role has to have these two permissions in your policy json.

```javascript
{  
   "Version":"2012-10-17",
   "Statement":[  
      {  
         "Effect":"Allow",
         "Action":[  
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents"
         ],
         "Resource":"arn:aws:logs:*:*:*"
      },
      {  
         "Sid":"InvokePermission",
         "Effect":"Allow",
         "Action":[  
            "lambda:InvokeFunction"
         ],
         "Resource":"*"
      },
      {  
         "Effect":"Allow",
         "Action":[  
            "S3:*"
         ],
         "Resource":[  
            "arn:aws:s3::*"
         ]
      }
   ]
}


```
