import boto3
import os
import sys
import uuid
import csv
import io
import pandas as pd
import functions_ABC 
import botocore


BUCKET_NAME = 'tcss562cloud' # replace with your bucket name
KEY = 'UWTacoma/credit_card_short.csv' # replace with your object key
FILENAME = 'credit_card_short.csv'
OUTPUT_KEY = 'UWTacoma/credit_card_short_ABC.csv'


def handler(event, context):
    s3 = boto3.resource('s3')
    upload_path = '/tmp/transformed-{}'.format(FILENAME)
    download_path = '/tmp/{}{}'.format(uuid.uuid4(), ".csv")
    s3.Bucket(BUCKET_NAME).download_file(KEY, download_path)
    df = pd.read_table(download_path, sep=",")
    print("File read finished...")
    t_abc = functions_ABC.TransformationABC(df, upload_path)
    t_abc.function_A()
    t_abc.function_B()
    t_abc.function_C()
    t_abc.save_csv()
    print("upload path:", upload_path)
    data = open(upload_path, 'rb')
    s3.Bucket(BUCKET_NAME).put_object(Key=OUTPUT_KEY, Body=data)
    print("upload to S3 finished...")
    return "Transformation finished!"
