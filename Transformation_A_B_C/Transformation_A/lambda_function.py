import boto3
import os
import sys
import uuid
import csv
import io
import pandas as pd
import functions_A 
import botocore
import json
import base64
from myconfig import *

def handler(event, context):
    s3 = boto3.resource('s3')
    upload_path = '/tmp/transformed-{}{}'.format(uuid.uuid4(), FILENAME)
    download_path = '/tmp/{}{}'.format(uuid.uuid4(), ".csv")
    s3.Bucket(BUCKET_NAME).download_file(KEY, download_path)
    df = pd.read_table(download_path, sep=",")
    print("Transformation A: File read finished...")
    t_a = functions_A.TransformationA(df, upload_path)
    t_a.function_A()
    t_a.save_csv()
    data = open(upload_path, 'rb')
    s3.Bucket(BUCKET_NAME).put_object(Key=OUTPUT_KEY, Body=data)
    print("Transformation A: upload to S3 finished...")
    print("TransformationA finished!")
    os.remove(upload_path) if os.path.exists(upload_path) else print("{} not deleted".format(upload_path))
    os.remove(download_path) if os.path.exists(download_path) else print("{} not deleted".format(download_path)) 
    lm = boto3.client('lambda')
    try:
        response = lm.invoke(
            FunctionName=FUNCTION_NAME,
            InvocationType='RequestResponse',
            LogType='Tail')
        print(base64.b64decode(response['LogResult']))
    except Exception as e:
        print(e)
        raise(e)
    print("TransformationA_B_C finished")
    r = {}
    payload = (response['Payload'].read().decode())
    d = json.loads(json.loads(payload))
    r['PayloadC'] = d['PayloadC']
    r['PayloadB'] = response['LogResult']
    return json.dumps(r)

