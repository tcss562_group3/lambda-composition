import numpy as np 
import os
import pandas as pd 
import datetime

class TransformationB():
    
    def __init__(self, dataframe,csv_path):
        self.df = dataframe
        self.csv_path = csv_path

    def function_B(self):
        print("I am function B, I will filter dataframe such that output_a is always bigger than 0.1")
        start = datetime.datetime.now()
        print("Number of rows before filter:" , len(self.df))
        self.df = self.df.loc[self.df['output_a'] > 0.1]
        print("Number of rows after filter:" , len(self.df))
        elapsed = datetime.datetime.now() - start
        msg = "Function B execution took: %s milliseconds " % (elapsed.microseconds * 0.001)
        print(msg)
        
    def save_csv(self):
        print("I save the CSV to temp folder")
        self.df.to_csv(self.csv_path)