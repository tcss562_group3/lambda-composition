#!/bin/bash
export AWS_DEFAULT_OUTPUT="json"
totalruns=$1

if [[ $# -eq 0 ]] ; then
    echo 'Usage: ./get_metric.sh <testCases>'
    exit 1
fi


parse_json () {
   inputJson=$1
   #echo "$inputJson"
   output="$(echo $inputJson | jq -r . | jq -r .PayloadC)"
   output="$(echo $output | base64 --decode)"
   #output="$(echo $output | base64 --decode)"
   #output="${output##*$'END'}"
   echo "$output"
}

invoke_lambda () {
   json={}
   function_name=$1
   region=$2
   output=$(aws lambda invoke --invocation-type RequestResponse --function-name $function_name --region $region --log-type Tail --payload $json --output json out.txt)
   #echo "$output"
   #output=$(aws lambda invoke --invocation-type RequestResponse --function-name $function_name --region $region --log-type Tail --payload $json out.txt | base64 --decode)
   #output="$(echo $output | jq -r .LogResult)"
   #output="$(echo $output | base64 --decode)"
   #output="${output##*$'REPORT'}"
   #decodedoutput=$(parse_json $output)
   echo "$output"
} 

extract_metrics () {
   line=$1
   function_name=$2
   performance=`echo $line | cut -d ' ' -f 4 `
   billedDuration=`echo $line | cut -d ' ' -f 8 `
   billedMemory=`echo $line | cut -d ' ' -f 12 `
   usedMemory=`echo $line | cut -d ' ' -f 17 `
   echo "$i,$function_name,$performance,$usedMemory,$billedDuration,$billedMemory"
   echo "$i,$function_name,$performance,$usedMemory,$billedDuration,$billedMemory" >> metric.csv
} 

for (( i=1 ; i<=$totalruns ; i++ ))
   do
    
    region="us-west-2"

    ## Composition A_B_C ##
    echo "Starting my target function"

    function_name="Transformation__A"
    
    output=$(invoke_lambda $function_name $region)
    #echo "$output"
    #extract_metrics "$output" $function_name
    
    sed 's/\\//g' out.txt > test.txt
    outputBC=`cat out.txt`
    outputB=$(echo "$outputBC" | jq -r . | jq -r .PayloadB)
    echo "$outputB"
    #decodedoutput=`echo $outputBC | tr -d '\\'`
    #echo "$decodedoutput"
    #decodedoutput="$(parse_json $outputBC)"

    #echo "$decodedoutput"
    

   done

#call from aws lambda cli
#aws lambda invoke --invocation-type RequestResponse --function-name lambda_composition --region us-west-2 --log-type Tail --payload '{"calcs":100000,"sleep":0,"loops":20}' out.txt | base64 --decode

#aws cloudwatch list-metrics --namespace "AWS/Lambda"
#echo "aws cloudwatch get-metric-statistics --metric-name Invocations --start-time 2018-05-04T22:48:00 --end-time 2018-05-04T22:53:00 --period 60 --namespace AWS/Lambda --show-request"
#aws cloudwatch get-metric-statistics --metric-name "Invocations" --start-time 2018-05-04T22:48:00 --end-time 2018-05-04T22:53:00 --period 60 --namespace AWS/Lambda --statistics SampleCount --dimensions '[{"Name": "lambda_composition","Value": "FunctionName"}]'

#aws cloudwatch get-metric-statistics --metric-name Invocations --start-time 2018-05-04T22:48:00 --end-time 2018-05-04T22:53:00 --period 60 --namespace AWS/Lambda --statistics SampleCount --dimensions '[{"Name": "lambda_composition","Value": "FunctionName"}]'

exit


