import boto3
import os
import sys
import uuid
import csv
import io
import pandas as pd
import functions_A 
import botocore
import json
import base64
from myconfig import *


def handler(event, context):
    s3 = boto3.resource('s3')
    upload_path = '/tmp/transformed-{}{}'.format(uuid.uuid4(), FILENAME)
    download_path = '/tmp/{}{}'.format(uuid.uuid4(), ".csv")
    s3.Bucket(BUCKET_NAME).download_file(KEY, download_path)
    df = pd.read_table(download_path, sep=",")
    print("File read finished...")
    t_a = functions_A.TransformationA(df, upload_path)
    t_a.function_A()
    t_a.save_csv()
    print("upload path:", upload_path)
    data = open(upload_path, 'rb')
    s3.Bucket(BUCKET_NAME).put_object(Key=OUTPUT_KEY, Body=data)
    print("upload to S3 finished...")
    print("TransformationA finished!")
    os.remove(upload_path) if os.path.exists(upload_path) else print("{} not deleted".format(upload_path))
    os.remove(download_path) if os.path.exists(download_path) else print("{} not deleted".format(download_path)) 
    lm = boto3.client('lambda')
    try:
        response = lm.invoke(
            FunctionName=FUNCTION_NAME,
            InvocationType='RequestResponse',
            LogType='Tail')
        print(base64.b64decode(response['LogResult']))
    except Exception as e:
        print(e)
        raise(e)
    print("TransformationBC also finished")
    r = "'%s' : '%s'" %('LogResult', response['LogResult'])
    print()
    return json.dumps(r)

