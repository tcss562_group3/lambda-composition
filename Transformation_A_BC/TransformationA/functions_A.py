import numpy as np 
import os
import pandas as pd 
import datetime

class TransformationA():
    
    def __init__(self, dataframe,csv_path):
        self.df = dataframe
        self.csv_path = csv_path

    def function_A(self):
        print("I am function A, I add V1, V2, V3 columns and then substract V4")
        start = datetime.datetime.now()
        self.df['output_a']= self.df.iloc[:, 1:4].sum(axis=1)
        self.df['output_a'] =  self.df['output_a'].sub(self.df['V4'], axis=0)
        elapsed = datetime.datetime.now() - start
        msg = "Function A execution took: %s milliseconds " % (elapsed.microseconds * 0.001)
        print(msg)
        
    def save_csv(self):
        print("I save the CSV to temp folder")
        self.df.to_csv(self.csv_path)
