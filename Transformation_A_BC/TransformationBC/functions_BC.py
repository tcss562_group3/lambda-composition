import numpy as np 
import os
import pandas as pd 
import datetime

class TransformationBC():
    
    def __init__(self, dataframe,csv_path):
        self.df = dataframe
        self.csv_path = csv_path

    def function_B(self):
        print("I am function B, I will filter dataframe such that output_a is always bigger than 0.1")
        start = datetime.datetime.now()
        self.df = self.df.loc[self.df['output_a'] > 0.1]
        elapsed = datetime.datetime.now() - start
        msg = "Function B execution took: %s milliseconds " % (elapsed.microseconds * 0.001)
        print(msg)

    def function_C(self):
        print("I am function C, I take mean of V1 to V4 columns and save it to vmean column")
        start = datetime.datetime.now()
        self.df['vmean'] =  self.df.iloc[:, 1:5].mean(axis=1)
        elapsed = datetime.datetime.now() - start
        msg = "Function C execution took: %s milliseconds " % (elapsed.microseconds * 0.001)
        print(msg)
        
    def save_csv(self):
        print("I save the CSV to temp folder")
        self.df.to_csv(self.csv_path)