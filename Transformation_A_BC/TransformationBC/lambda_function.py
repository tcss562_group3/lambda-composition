import boto3
import os
import sys
import uuid
import csv
import io
import pandas as pd
import functions_BC 
import botocore
import json
import base64
from myconfig import *

def handler(event, context):
    s3 = boto3.resource('s3')
    upload_path = '/tmp/transformed-{}{}'.format(uuid.uuid4(), FILENAME)
    download_path = '/tmp/{}{}'.format(uuid.uuid4(), ".csv")
    s3.Bucket(BUCKET_NAME).download_file(KEY, download_path)
    df = pd.read_table(download_path, sep=",")
    print("TransformationBC: File read finished...")
    t_bc = functions_BC.TransformationBC(df, upload_path)
    t_bc.function_B()
    t_bc.function_C()
    t_bc.save_csv()
    data = open(upload_path, 'rb')
    s3.Bucket(BUCKET_NAME).put_object(Key=OUTPUT_KEY, Body=data)
    print("TransformationBC: upload to S3 finished...")
    os.remove(upload_path) if os.path.exists(upload_path) else print("{} not deleted".format(upload_path))
    os.remove(download_path) if os.path.exists(download_path) else print("{} not deleted".format(download_path)) 
    return "TransformationBC finished!"
